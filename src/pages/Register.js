import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import {Navigate, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Register() {
	const {user} = useContext(UserContext)
	const history = useNavigate()
	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [password1, setPassword1] = useState('')

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && mobileNo.length > 10 && email !== '' && password !== '' && password1 !== '') && (password === password1)) {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	},
	[firstName, lastName, mobileNo, email, password, password1])

	// Check if values are successfully binded
	// console.log(email)
	// console.log(password1)
	// console.log(password2)
	

	const inputUser = () => {
		fetch('http://localhost:4000/users/register', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				mobileNo : mobileNo,
				email : email,
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)	
		})

		Swal.fire({
			title : "Successfully registered.",
			icon : "success",
			text : "Welcome to Zuitt!"
		})

		history("/login")
		// Clear input fields
		setEmail('')
		setPassword('')
		setPassword1('')
	}

	const registerUser = e => {
		e.preventDefault()
		fetch('http://localhost:4000/users/checkEmail', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},body : JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				mobileNo : mobileNo,
				email : email,
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				return (
					Swal.fire({
						title : "Email is already registered.",
						icon : "error",
						text : "Please use another email."
					})
				)
			}
			else {
				inputUser()
			}
		})
	}

	return (
		(user.id !== null) ?
			<Navigate to="/login" />
		:
		<Form onSubmit={e => registerUser(e)}>
			<h1>Register</h1>
			<Form.Group className="mb-3" controlId="userFName">
				<Form.Label>First Name</Form.Label>
				<Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)} required />
			</Form.Group>

			<Form.Group className="mb-3" controlId="userLName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={e => setLastName(e.target.value)} required />
			</Form.Group>

			<Form.Group className="mb-3" controlId="userMNumber">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control type="text" placeholder="Enter mobile number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required />
			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required />
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required />
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required />
			</Form.Group>

			{/* Conditionally render submit button based on isActive state */}
			{ isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
			:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			}
		</Form>
	)
}