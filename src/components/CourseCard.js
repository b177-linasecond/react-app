import {Row, Col, Card, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {useState, useContext, useEffect} from 'react'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'

export default function CourseCard({courseProp}) {
	// console.log(props)
	// console.log(typeof props)

	const {_id, name, description, price} = courseProp

	// Use the state hook for this component to be able to store its state
	// State are used to keep track of information related to individual components
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue)

	// const [count, setCount] = useState(0)
	// const [seat, setSeat] = useState(30)

	// console.log(useState(0))

	// Function that keeps track of enrolless for a course
	// const enroll = () => {
	// 	setCount(count + 1)
	// 	console.log(`Enrollees ${count}`)
	// }

	// const seats = () => {
	// 	if(seat == 0) {
	// 		alert(`No more seats available.`)
	// 	}
	// 	else {
	// 		setSeat(seat - 1)
	// 		enroll()
	// 	}
	// }

	return (
		<Row className="mt-3 mb-3">
			<Col>
				<Card>
				  <Card.Body className="p-3 m-0">
				    <Card.Title className="title m-0 p-0">
				    	{name}
				    </Card.Title>
				    <Card.Subtitle className="description">
				    	Description:
				    </Card.Subtitle>
				    <Card.Text>
				    	{description}
				    </Card.Text>
				    <Card.Subtitle className="description">
				    	Price:
				    </Card.Subtitle>
				    <Card.Text>
				    	PhP {price}
				    </Card.Text>
				    <Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}

// Check if the CourseCard component is getting the correct prop types

CourseCard.propTypes = {
	courseProp : PropTypes.shape({
		name : PropTypes.string.isRequired,
		description : PropTypes.string.isRequired,
		price : PropTypes.number.isRequired
	})
}