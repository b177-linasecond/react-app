import {Button, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function PageNotFound() {
	return (
		<Row>
			<Col className="p-5">
				<h1>Page Not Found</h1>
				<p>Go back to <Link as={Link} to="/">homepage.</Link></p>
			</Col>
		</Row>
	)
}