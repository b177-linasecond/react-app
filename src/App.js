import {Fragment, useState, useEffect} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Routes} from 'react-router-dom'
import './App.css';
import AppNavbar from './components/AppNavbar'
import Register from './pages/Register'
import Login from './pages/Login'
import Home from './pages/Home'
import Courses from './pages/Courses'
import CourseView from './components/CourseView'
import PageNotFound from './components/PageNotFound'
import Logout from './pages/Logout'
import {UserProvider} from './UserContext'

function App() {
  // State hook for the user state that's defined for a global scope
  const [user, setUser] = useState({
    // email : localStorage.getItem('email')
    id : null,
    isAdmin : null
  })

  // Function clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored during login and localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/courses" element={<Courses />} />
            <Route exact path="/courses/:courseId" element={<CourseView />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="*" element={<PageNotFound />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
