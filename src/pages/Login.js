import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import {Navigate} from 'react-router-dom'
import Swal from 'sweetalert2'

const Login = () => {
	// Allows us to consume the User context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	},
	[email, password])


	const loginUser = e => {
		e.preventDefault()

		// Process a fetch request to the corresponding backend API
		/* Syntax:
			fetch('url', {options})
			.then(res => res.json())
			.then(data => {})

		*/
		
		fetch('http://localhost:4000/users/login', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				email : email,
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== "undefined") {
				// The JWT will be used to retrieve user information
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
				// Sweet alert message
				Swal.fire({
					title : "Successfully logged in.",
					icon : "success",
					text : "Welcome to Zuitt!"
				})
			}
			else {
				Swal.fire({
					title : "Authentication failed.",
					icon : "error",
					text : "Check your login details and try again."
				})
			}
		})

		// Set the email of the user in the local storage
		/* Syntax:
			localStorage.setItem('propertyName', value)
		*/

		// localStorage.setItem('email', email)

		// Set the global user state to have properties obtained from local storage
		// setUser({
		// 	email : localStorage.getItem('email')
		// })

		setEmail('')
		setPassword('')

		// alert(`You are now logged in!`)
	}

	// "retrieveUserDetails" function to convert JWT from fetch request
	const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/users/details', {
			headers : {
				Authorization : `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id : data._id,
				isAdmin : data.isAdmin
			})
		})
	}

	return (
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
		<Form onSubmit={e => loginUser(e)}>
			<h1>Login</h1>
			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required />
			</Form.Group>

			<Form.Group className="mb-3" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required />
			</Form.Group>

			{ isActive ?
				<Button variant="success" type="submit" id="submitBtn">
					Login
				</Button>
			:
				<Button variant="success" type="submit" id="submitBtn" disabled>
					Login
				</Button>
			}
		</Form>
	)
}

export default Login