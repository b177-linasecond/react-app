import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import {Link, NavLink} from 'react-router-dom'
import React, {useState, useContext} from 'react'
import UserContext from '../UserContext'

const AppNavbar = () => {
	// State to store the user information stored in the login page
	// const [user, setUser] = useState(localStorage.getItem('email'))
	// console.log(user)

	const {user} = useContext(UserContext)

	return (
		<Navbar bg="light" expand="lg">
		    <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      	<Nav className="me-auto">
		        	<Nav.Link as={Link} to="/">Home</Nav.Link>
		        	<Nav.Link as={Link} to="/courses">Courses</Nav.Link>
		        	{(user.id !== null) ?
		        		<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
		        		:
		        		<React.Fragment>
		        			<Nav.Link as={Link} to="/login">Login</Nav.Link>
		        			<Nav.Link as={Link} to="/register">Register</Nav.Link>
		        		</React.Fragment>
		        	}
		        	
		      	</Nav>
		    </Navbar.Collapse>
		</Navbar>
	)
}

export default AppNavbar